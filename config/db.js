// 5.1 En el archivo db.js importar Sequelize es un objeto y su nombre de objeto se escribe en mayusculas
const Sequelize = require("sequelize");

// 5.6 Importar una libreria llamada dotenv en el archivo db
require("dotenv").config({ path: "variables.env"});

// 5.2 Establecer los parametros de la conexion a la db
const db = new Sequelize(
    "dbPrueba",
    process.env.MYSQLUSER,
    process.env.MYSQLPASS,
    {
        host: "localhost",
        dialect: "mysql",
        port: process.env.PORT,
        operatorAliases: false,
        define: {
            timestamps: false
        },
        pool: {
            max: 10,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    }
);

// 5.3 Emportar la informacion para mandarla a llamar desde otro lado
module.exports = db;