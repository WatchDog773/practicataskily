// 4.1 Crear una funcion ene l controlador para las rutas
// Obsoleto solo funciono para prueba
// exports.home = (req, res, next) => {
//     res.send("Bienvenidos");
// }

// 8.2 Exportar los modelos necesarios
const Proyecto = require("../models/Proyecto")

// 7.8 En el proyectos controller renderisamos con res el crear_proyecto
exports.home = (req, res, next) => {
    res.render("crear_proyecto");
};

// 8.3 Luego en el archivo proyectosController vamos a hacer la funcion para que el devuelva algo y a la vez permite la creacion de un nuevo proyecto
// Obsoleto era para probar siempre hacer antes del paso 8.4
// exports.nuevoProyecto = (req, res, next) => {
//     // 8.7 hacer un console log en el proyectos controller para verificar si esta enviando
//     console.log(req.body);

//     // Esto va en el paso 8.3
//     res.send("Aqui se crea un nuevo proyecto");
// };

exports.nuevoProyecto = (req, res, next) => {
    // 8.9 Vamos a validar el input del formulario, para acceder a los valores y asingnarlos en un solo paso vamos a utilizar destructuring
    const { nombre } = req.body;

    // 9.1 creamos un arreglo
    const errores = [];

    // 8.10 verificar si el proyecto tiene un valor
    if(!nombre) {
        // 9.2 agregamos el error al arreglo con push
        errores.push({ error: "El nombre del proyecto no debe de ser vacio"});
    }

    // 9.3 si hay errores
    if (errores.length)
    {
        res.render("crear_proyecto", {
            errores
        });
    }
    else
    {
        // 9.4 si no hay errores debe de insertar el proyecto a la base de datos
        res.send("Insertado en la base de datos");
    }
};