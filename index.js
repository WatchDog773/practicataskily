// 1.1 Importar los modulos de express
const express = require("express");

// 7.1 Importar handlebars antes de las rutas en el index principal
const exphbs = require("express-handlebars");

// 8.5 se necesita la manera de leer en el cuerpo donde envia la peticion y esto lo haremos en el index principal con bodyParse
const bodyParse = require("body-parser");

// 2.3 Importar rutas disponibles
const routes = require("./routes");

// 5.7 En el index principal importar la conexion a la base de datos
const db = require("./config/db");

// 6.14 el modleo que creamo nos vamos al archivo index principal e importamos los modelos
require("./models/Proyecto");

// 5.8 Realizar la conexion al servidor en index principal con la promise
db.sync()
    .then(() => console.log("Se conecto con el servidor de DB"))
    .catch((error) => console.log(error));

// 1.2 Crear una servidor de express
const app = express();

// 7.2 Indicar el template engine a utilizar (Handlebars)
app.engine(
    "hbs", 
    exphbs({
    defaultLayout: 'main', // 7.3 Aqui se llama al main template de layouts
    extname: ".hbs"
})
);

// 7.4 luego decirle al servidor con app
app.set("view engine", "hbs");

// 8.6 despues del template engine vamos a habilitar bodyParser
app.use(bodyParse.urlencoded({ extended: true }));

// 2.4 Indicarle a express las rutas del servidor
app.use("/", routes());

// 1.3 Inicializar el servidor
app.listen(7000,  () => {
    console.log("Servidor ejecutandose en el puerto 7000");
});