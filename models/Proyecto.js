// 6.1 En ese archivo es importar mysql y sequelize es un objeto y va con mayuscula
const Sequelize = require("sequelize");

// 6.2 Importar la configuracion de la base de datos trayendo el archivo db
const db = require("../config/db");

// 6.9 Importar slug en el archivo proyecto de la carpeta models
const slug = require("slug");

// 6.10 Importar shortid en el archivo proyecto de la carpeta models
const shortid = require("shortid");

// 6.3 Crearemos un modelo aquis e crean las tablas de la base
const Proyecto = db.define("proyecto", { // 6.4 El nombre de la tabla y luego en llaves sus campos
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nombre: {
        type: Sequelize.STRING
    },
    url: {
        type: Sequelize.STRING
    },
}, {
        // 6.7 Agregar un hook
        hooks: {
            // 6.8 Hook beforeCreate para enviarle los valores del proyecto
            beforeCreate(proyecto) {
                console.log("Antes de insertar en la base de datos");
                
                // 6.11 declarar una constante media slug el nombre dle proyecto y lo hacemos minusculas
                const url = slug(proyecto.nombre).toLowerCase();

                // 6.12 Al proyecto se le va a concatenar la url del proyecto y un id con shortd
                proyecto.url = `${url}_${shortid.generate()}`;
            },

            // 6.13 Agregamos otro hoock de beforeUpdate por si el usuario actualiza rl proyecto
            beforeUpdate(proyecto) {
                console.log("Antes de actualizar en la base");
                const url = slug(proyecto.nombre).toLowerCase();
                proyecto.url = `${url}_${shortid.generate()}`;
            }
        }
});

// 6.5 Importar el modelo para utilizarlo
module.exports = Proyecto;