// 2.1 Importar express router
const express = require("express");
const routes = express.Router();

// 4.2 Importar en el index de las rutas el controlador del proyecto llamando al archivo proyectos constrollers
const proyectosController = require("../controllers/proyectosController");

// 2.2 Construir las rutas disponibles en el serivor
module.exports = function() {
    routes.get('/', proyectosController.home);

    // 8.1 Usando los verbos http en este caso POST se encarga de crear un nuevo proyecto como una respuesta, todo esto en el index de rutas
    routes.post("/nuevoProyecto", proyectosController.nuevoProyecto);

    return routes;
};